#include <stdio.h>
#include <string.h>

void concat(char s1[], char s2[]) {
    strcat(s1, s2);
}

void removeStdChar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

int main() {
    char s1[255];
    char s2[255];
    printf("Enter the first string:");
    fgets(s1, sizeof(s1) * sizeof(char), stdin);
    removeStdChar(s1);
    s1[strlen(s1) - 1] = ' ';
    printf("Enter the second string:");
    fgets(s2, sizeof(s2) * sizeof(char), stdin);
    removeStdChar(s2);
    concat(s1, s2);
    printf("The concatenated string: %s\n", s1);
    return 0;
}